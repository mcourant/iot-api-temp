import env from "getenv"

export default {
    version: "0.0.1",
    config: {
        "port": env.int('SERVER_PORT', 3031),
        "domain": env.string('SERVER_DOMAIN', 'localhost'),
        "env_project": env.string("ENV_PROJECT", "dev"),
    },
    sql: {
        host: env.string("SQL_HOST", "192.168.99.100"),
        user: env.string("SQL_USER", "postgresadmin"),
        password: env.string("SQL_PASSWORD", "admin123"),
        database: env.string("SQL_DATABASE", "postgresdb"),
        port: env.string("SQL_PORT", 31194)
    }
}
