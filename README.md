# Documentation

Ce projet consiste a afficher les différentes valeurs entre la temperature et l'humidité récupéré dupuis un module connecté a un raspberry pi (DHT11).

# Api

Ce projet correspond a l'api hébergé sur le raspberry pi qui lui même est connecté a un capteur de température et d'humidité. 
Elle utilise principalement la library "node-dht-sensor" pour récupérer les informations sous forme de Json :
```json
{
    "temperature" : 27
    "humidity": 40
}
```