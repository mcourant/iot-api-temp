const sensor = require('node-dht-sensor');
 
export const getDatas = (req, res) => {
    sensor.read(11, 4, function(err, temperature, humidity) {
        if (!err) {
            res.send({
                error:false,
                data: {
                    humidity: humidity.toFixed(1),
                    temperature: temperature.toFixed(1)
                }
            });
        }else{
            res.send({
                error: 'No data',
                data:{}
            });
        }
    });
}
