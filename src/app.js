import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

// Configuration
import config from '../config';

import {getDatas} from './routes/capteurTH';

// Welcome
console.log('Teester Media API ' + config.version);

// Init our express app
const app = express();

// Parse JSON body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status >= 400 && err.status < 500 && err.message.indexOf('JSON'))
        res.status(400).send({ error: { code: 'INVALID_BODY', message: 'Syntax error in the JSON body' } });
});

// CORS
app.use(cors());

// Init routes
console.log('Initializing API routes...');
app.get('/datas', getDatas);

app.get('/', (req,res) => {
    res.send({
        status : "online",
        version: "1.0.0"
    })
});

// Bind express on the port
app.listen(config.config.port);
console.log('API was launched on port ' + config.config.port);
